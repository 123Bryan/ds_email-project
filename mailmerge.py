"""
Documantion is handy to have incase you forget how something works.

> python mailmerge.py subject.txt message.txt data.csv --send

Where subject.txt looks like:

About your audition

Where message.txt looks like:

Hey {name},

Thanks your interested in {part}.

We will get back to you with in a week.

You

and data.csv looks like:

name,part,email
Albert,Hamlet,albert@example.com
Colin,Hamlet,colin@example.com

The program will ask you for a gmail address and password and then send the
messages on your behalf.

If you don't include --send the emails it would send will be printed to the
commandline so you can make sure things are working right.
"""

import sys
import os
import csv
import smtplib
from getpass import getpass


usage = 'python mailmerge.py subject.txt message.txt data.csv [--send]'


def print_email(from_email, to_email, subject, message):
    template = "{0} {1} {2} {3}"
    print(template.format(from_email, to_email, subject, message))


def send_email(server, from_email, to_email, subject, message):
    headers = 'To: {0}\r\nFrom: {1}\r\nSubject: {2}\r\n\r\n'
    headers = headers.format(to_email, from_email, subject)
    server.sendmail(from_email, to_email, headers + message)  


def create_server(username, password):
    server = smtplib.SMTP('smtp.gmail.com:587')
    server.ehlo()
    server.starttls()
    server.ehlo()
    server.login(username, password)
    return server


if __name__ == '__main__':
    print("Hello, World!")
    print("blam!")
    print(sys.argv)


if len(sys.argv) < 4:
    print(usage)
    sys.exit(0)
    
subject_file_name = sys.argv[1]
message_file_name = sys.argv[2]
csv_file_name = sys.argv[3]

if not os.path.isfile(subject_file_name):
    print("{0} is not a file.".format(subject_file_name))
    sys.exit(0)
# Validate the rest are files
if not os.path.isfile(subject_file_name):
    print("{0} is not a file.".format(message_file_name))
    sys.exit(0) 


with open(subject_file_name, "r") as subject_file:
    subject = subject_file.read()
print(subject)
with open(message_file_name, "r") as message_file:
    message = message_file.read()
print(message)


from_email = raw_input("Your Gmail address: ")
with open(csv_file_name, "r") as csv_file:
    rows = csv.DictReader(csv_file)
    count = 0
    for row in rows:
        print(row)
        count = count + 1

        print_email(from_email,
                row['email'],
                subject.format(**row),
                message.format(**row))


send = False
if len(sys.argv) == 5 and sys.argv[4] == "--send":
    send = True
    password = getpass()
    server = create_server(from_email, password)



if send:
    send_email(server,
               from_email,
               row['email'],
               subject.format(**row),
               message.format(**row))
else:
    print_email(from_email,
                row['email'],
                subject.format(**row),
                message.format(**row))










